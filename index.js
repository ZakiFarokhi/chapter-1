const axios = require('axios')

getDataAsyncAwait();
// getDataCallback();

async function getDataAsyncAwait(){
    try{
        const response = await axios('https://indonesia-public-static-api.vercel.app/api/heroes');
        const data = response.data; //array object
        console.log(data[0])
    }catch(err){
        console.error(err.message)
    }
}

function getDataCallback(){
    axios.get('https://indonesia-public-static-api.vercel.app/api/heroes')
    .then(response => {
        console.log(response.data)
    }).catch(err => {
        console.error(err)
    })
}